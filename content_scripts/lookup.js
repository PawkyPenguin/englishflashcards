(function() {
        if (window.hasRun) {
                return;
        }
        window.hasRun = true;

        function mouseUp(e) {
                translationDiv.removeEventListener('mousemove', moveDiv);
        }

        function mouseDown(e) {
                translationDiv.addEventListener('mousemove', moveDiv);
        }

        function moveDiv(e) {
                translationDiv.style.left = e.clientX + 'px';
                translationDiv.style.top = e.clientY + 'px';
        }

        console.log("Hello from lookup.js");
        var isDivVisible = false;
        var selectedTerm = "";
        var germans = [];
        var englishs = [];
        const translationDiv = document.createElement("div");
        translationDiv.className = "translationDiv";
        translationDiv.id = "englishLookupDiv";
        const translationTable = document.createElement("table");
        translationTable.className = "translationTable";
        translationTable.id = "englishLookupTable";
        translationDiv.appendChild(translationTable);
        document.body.appendChild(translationDiv);

        //translationDiv.addEventListener('mousedown', mouseDown, false);
        //document.addEventListener('mouseup', mouseUp, false);

        document.addEventListener("click", (e) => {
                function addCSS(string) {
                        const style = document.createElement('style');
                        style.textContent = string;
                        document.head.append(style);
                }

                function getSelected() {
                        return escape(window.getSelection().toString());
                }

                function toggle(checkbox) {
                                checkbox.checked = !checkbox.checked;
                }

                function appendTranslation(german, english, id) {
                        console.log("appending '" + german + "' and '" + english + "'.");
                        var checkboxId = "translationId" + id;
                        var checkboxTd = document.createElement("td");
                        checkboxTd.className = "checkboxTd";
                        var checkbox = document.createElement("input");
                        checkbox.type = "checkbox";
                        checkbox.id = checkboxId;
                        checkbox.className = "checkboxEntry";

                        checkboxTd.appendChild(checkbox);
                        checkboxTd.addEventListener("click", (e) => {
                                checkbox.checked = !checkbox.checked;
                        });

                        var td1 = document.createElement("td");
                        var td2 = document.createElement("td");
                        td1.appendChild(document.createTextNode(german));
                        td2.appendChild(document.createTextNode(english));
                        var r = document.createElement("tr");
                        r.appendChild(checkboxTd);
                        r.appendChild(td1);
                        r.appendChild(td2);
                        translationTable.appendChild(r);
                }

                function clearNode(node) {
                        var last;
                        while (last = node.lastChild) {
                                node.removeChild(last);
                        }
                }

                function getEntryText(element) {
                        var entry = "";
                        for (j = 0; j < element.children.length; j++) {
                                var text = element.children[j].text;
                                if (!text) {
                                        continue;
                                }
                                entry += text + " ";
                        }
                        return entry;
                }

                function cleanWord(word) {
                        word = word.replace(/\[[^\]]*\]/g,'');
                        word = word.replace(/{[^}]*}/g,'');
                        return word;
                }

                function displayResults(xmlResponse) {
                        clearNode(translationTable);
                        germans = [];
                        englishs = [];
                        for (i = 1; i < 5; i++) {
                                let row = xmlResponse.getElementById("tr" + i);
                                if (!row) {
                                        break;
                                }
                                let contents = row.getElementsByClassName("td7nl");
                                let germanEntry = contents[1];
                                let englishEntry = contents[0];
                                let german = getEntryText(germanEntry);
                                let english = getEntryText(englishEntry);
                                germans.push(cleanWord(german));
                                englishs.push(cleanWord(english));
                                appendTranslation(german, english, i)
                        }
                        translationDiv.style.left = e.pageX + 'px';
                        translationDiv.style.top = e.pageY + 'px';
                        translationDiv.style.visibility = "visible";
                        isDivVisible = true;
                }

                function closeDiv() {
                        if (!isDivVisible) {
                                return;
                        }
                        translationDiv.style.visibility = "hidden";
                        isDivVisible = false;

                        mineCheckedEntries();
                }

                function mineCheckedEntries() {
                        var entries = document.querySelectorAll('input[type="checkbox"].checkboxEntry');
                        var i = 0;
                        for (const entry of entries) {
                                console.log(entry);
                                if (entry.checked) {
                                        var german = germans[i];
                                        var english = englishs[i];
                                        var command = {
                                                "action": "addNote",
                                                "version": 6,
                                                "params": {
                                                        "note": {
                                                                        "deckName": "mined",
                                                                        "modelName": "Language",
                                                                        "fields": {
                                                                                "Foreign": english,
                                                                                "Native": german,
                                                                                "Add Reverse": "y",
                                                                                "AlwaysDisplayed": ""
                                                                        },
                                                                        "tags": []
                                                                }
                                                }
                                        };
                                        browser.runtime.sendMessage({
                                                type: "ankimine",
                                                command: JSON.stringify(command)
                                        });
                                }
                                i += 1;
                        }
                }

                function sendRequest(term) {
                        var dictURL = "https://en-de.dict.cc/?s=" 
                        var queryURL = dictURL + term;
                        const request = new Request(queryURL)

                        var xhttp = new XMLHttpRequest();

                        xhttp.onload = function () {
                                displayResults(this.responseXML);
                        };

                        xhttp.open("GET", queryURL, true);
                        xhttp.responseType = "document";
                        xhttp.send();
                }

                var term = getSelected().trim().toLowerCase();
                if (!translationDiv.contains(e.target)) {
                        closeDiv();
                }
                if (term === "" || (isDivVisible && selectedTerm === term)) {
                        return;
                }
                selectedTerm = term;
                sendRequest(selectedTerm);
        });
})();
