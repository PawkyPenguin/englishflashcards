const host = "http://127.0.0.1:8765";
console.log("Hello from ankiConnection.js");

browser.runtime.onMessage.addListener((req, sender) => {
        if (req.type == "ankimine") {
                mineToAnki(req.command, sender);
        } else {
                console.log("Received:");
                console.log(req);
                console.log("from:");
                console.log(sender);
        }

});

function error(tab, error) {
        try {
                browser.tabs.sendMessage(tab, {type: "error", error: error});
        } catch (err) {
                console.log("couldn't send error " + error + " to tab " + tab);
        }
}

function respond(xhr) {
        let response = JSON.parse(xhr.responseText);
        if(response.error) {
                //send_error(sender.tab.id, "Couldn't mine: " + response.error);
                console.log("error: " + response.error);
        } else {
                console.log("fine");
                console.log(response);
        }
}

function mineToAnki(command, sender) {
        console.log("executing command: " + command);
        let xhr = new XMLHttpRequest();
        xhr.overrideMimeType('application/json');
        //xhr.setRequestHeader('Content-type','application/json; charset=utf-8');
        xhr.addEventListener('load', () => {respond(xhr);});
        xhr.addEventListener('error', (e) => {
                        error(sender.tab.id, "AnkiConnect mining failed (Anki is probably not open)");
                        console.log(e);
                });
        xhr.addEventListener('timeout', () => {
                        error(sender.tab.id, "AnkiConnect mining failed: timed out");
                console.log("timeout :(");
                });
        xhr.open("POST", host);
        //xhr.send(JSON.stringify({
        //        action: "deckNames",
        //        version: 6
        //}));
        xhr.send(command);
}
